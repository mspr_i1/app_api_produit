from django.db import models
from datetime import datetime
from django.conf import settings
from django.utils import timezone


# Create your models here.

class Product(models.Model):
    date = models.DateField(default=datetime.now)
    name = models.CharField(max_length=400)
    details = models.CharField(max_length=300)
    price = models.FloatField(max_length=200)
    description = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    stock = models.IntegerField()


class Supervision(models.Model):
    request_return_code = models.IntegerField()
    query_exec_time = models.FloatField()
    nb_request = models.IntegerField(default=1)
    exception_in_code = models.TextField(null=True, blank=True)
    timestamp = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"Request {self.pk} - {self.request_return_code}"
