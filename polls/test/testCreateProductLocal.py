# Create your tests here.
from datetime import datetime

from polls.models import Product
import unittest


class TestUniter(unittest.TestCase):
    def test_product(self):
        name = "café italien"
        details = "Intense,Céréales grillées"
        price = 25.28
        description = "Ce café est un pur arabica noir possédant des pointes fortes et allongés"
        color = "noir"
        stock = 2

        product = Product.objects.create(name=name, details=details, price=price,
                                         description=description, color=color,
                                         stock=stock)
        self.assertEqual(product.name, name)
        self.assertEqual(product.details, details)
        self.assertEqual(product.price, price)
        self.assertEqual(product.description, description)
        self.assertEqual(product.color, color)
        self.assertEqual(product.stock, stock)
