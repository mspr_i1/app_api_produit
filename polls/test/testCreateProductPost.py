from django.test import TestCase
from ..models import Product
from django.urls import reverse
from django.test.client import Client as APIProduct


class Test_create_product(TestCase):
    def test_post_create_product(self):
        # Création d'une instance de Client avec les données récupérées
        product = APIProduct()
        response = product.post(f'/polls/create_product/', {
            "name": "café italien",
            "details": "Intense,Céréales grillées",
            "price": "25.28",
            "description": "Ce café est un pur arabica noir possédant des pointes fortes et allongés",
            "color": "noir",
            "stock": "2"
        })

        # Vérifie que la réponse a un statut 200 OK
        self.assertEqual(response.status_code, 200)

        print(response.json())
        # Retournez une réponse de succès
