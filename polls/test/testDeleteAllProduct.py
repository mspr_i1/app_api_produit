from django.test import TestCase
from ..models import Product
from django.test.client import Client as APIProduct


class Test_deleteAllProduct(TestCase):
    def test_delete_all_product(self):
        Product.objects.create(
            name='café italien',
            details='Intense,Céréales grillées',
            price=25.28,
            description='Ce café est un pur arabica noir possédant des pointes fortes et allongés',
            color='noir',
            stock=2
        )
        Product.objects.create(
            name='café grec',
            details='Intense,Céréales grillées,yaourt',
            price=25.28,
            description='Ce café est un pur arabica noir possédant des pointes fortes et allongés',
            color='noir',
            stock=3
        )
        Product.objects.create(
            name='café lyonnais',
            details='Doux,léger',
            price=25.28,
            description='Ce café est un pur arabica noir possédant des pointes fortes et allongés',
            color='noir',
            stock=2
        )

        product = APIProduct()
        response = product.post(f'/polls/delete_products/')


        # Vérifie que la réponse a un statut 200 OKs
        self.assertEqual(response.status_code, 200)
