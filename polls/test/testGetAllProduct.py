from django.test import TestCase
from ..models import Product
from django.test.client import Client as APIProduct


class Test_getAllProductsDetails(TestCase):
    def test_get_product_details_by_all(self):
        Product.objects.create(
            name='café italien',
            details='Intense,Céréales grillées',
            price=25.28,
            description='Ce café est un pur arabica noir possédant des pointes fortes et allongés',
            color='noir',
            stock=2
        )
        Product.objects.create(
            name='café grec',
            details='Intense,Céréales grillées,yaourt',
            price=25.28,
            description='Ce café est un pur arabica noir possédant des pointes fortes et allongés',
            color='noir',
            stock=3
        )
        Product.objects.create(
            name='café lyonnais',
            details='Doux,léger',
            price=25.28,
            description='Ce café est un pur arabica noir possédant des pointes fortes et allongés',
            color='noir',
            stock=2
        )

        product = APIProduct()
        response = product.get(f'/polls/product/')

        # Vérifie que la réponse a un statut 200 OKs
        self.assertEqual(response.status_code, 200)
        self.maxDiff = None
        self.assertEqual(response.json(),  [
            {'id': 1, 'name': 'café italien', 'details': 'Intense,Céréales grillées','price': 25.28,
             'description': 'Ce café est un pur arabica noir possédant des pointes fortes et allongés',
             'color': 'noir', 'stock': 2},
            {'id': 2, 'name': 'café grec', 'details': 'Intense,Céréales grillées,yaourt', 'price': 25.28,
             'description': 'Ce café est un pur arabica noir possédant des pointes fortes et allongés',
             'color': 'noir', 'stock': 3},
            {'id': 3, 'name': 'café lyonnais', 'details': 'Doux,léger', 'price': 25.28,
             'description': 'Ce café est un pur arabica noir possédant des pointes fortes et allongés',
             'color': 'noir', 'stock': 2}
        ])
