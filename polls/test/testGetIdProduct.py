from django.test import TestCase
from ..models import Product
from django.urls import reverse
from django.test.client import Client as APIProduct


class TestGetProductByID(TestCase):
    def test_get_client_details_by_id(self):
        product = APIProduct()
        product_instance = Product.objects.create(
            name='Café Dosteyevsky',
            details='Complexe',
            price='47.30',
            description='Ce café chic du centre-ville de Moscou est le summum de l\'extravagance et du glamour.',
            color='noir',
            stock='2'
        )

        # Crée une requête GET pour récupérer les détails du client
        response = product.get(f'/polls/product/{product_instance.id}/')

        # Vérifie que la réponse a un statut 200 OK
        self.assertEqual(response.status_code, 200)

        # Affiche les données récupérées dans la console
        print(response.json())
