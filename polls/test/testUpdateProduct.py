from django.test import TestCase
from django.urls import reverse
from ..models import Product
from django.test.client import Client as APIProduct


class UpdateDataProductByIdTestCase(TestCase):
    def test_update_data_product_by_id(self):
        product = APIProduct()
        product_instance = Product.objects.create(
            name='café italien',
            details='Intense,Céréales grillées',
            price='25.28',
            description='Ce café est un pur arabica noir possédant des pointes fortes et allongés',
            color='noir',
            stock='2'
        )

        # Crée une requête GET pour récupérer les détails du product
        response = product.get(f'/polls/product/{product_instance.id}/')
        # Vérifie que la réponse a un statut 200 OK
        self.assertEqual(response.status_code, 200)

        update = product.post(f'/polls/update_data_by_id/{product_instance.id}/', {
            "name": "café russe",
            "details": "Intense,Vodka",
            "price": "32",
            "description": "Ce café est un pur arabica noir possédant des pointes fortes de vodka",
            "color": "marron",
            "stock": "2"
        })

        self.assertEqual(update.status_code, 200)

