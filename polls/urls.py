from django.urls import path
from .views import get_by_id
from .views.delete_all_products import delete_all_products
from .views.get_all_products import getAllProductsDetails
from .views.get_by_id import getProductDetailsById
from .views.create_product import create_product
from .views.delete_product_by_id import delete_data_product_by_id
from .views.update_data_by_id import update_data_product_by_id
urlpatterns = [
    path('product/<int:pk>/', getProductDetailsById),
    path('product/', getAllProductsDetails),
    path('create_product/', create_product.as_view()),
    path('delete_products/', delete_all_products),
    path('delete_product_by_id/<int:pk>/', delete_data_product_by_id),
    path('update_data_by_id/<int:pk>/', update_data_product_by_id)
]