from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
from ..models import Product
#from polls import rabbitMQ_sender
import json


@method_decorator(csrf_exempt, name='dispatch')
class create_product(View):
    def post(self, request, *args, **kwargs):
        try:
            # Récupération des données de la requête POST
            name = request.POST.get('name', '')
            details = request.POST.get('details', '')
            price = request.POST.get('price', '')
            description = request.POST.get('description', '')
            color = request.POST.get('color', '')
            stock = request.POST.get('stock', '')

            # Création d'une instance de Client avec les données récupérées
            product = Product.objects.create(

                name=name,
                details=details,
                price=price,
                description=description,
                color=color,
                stock=stock,

            )
            # Sauvegarde de l'instance dans la base de données
            product.save()
            #rabbitMQ_sender.send_message_to_rabbitmq(product)
            # Retournez une réponse de succès
            return JsonResponse({'message': 'Produit créé avec succès !'})
        except json.JSONDecodeError:
            return JsonResponse({'error': 'Données JSON invalides'}, status=400)
