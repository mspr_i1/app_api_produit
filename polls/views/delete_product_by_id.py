import json
import pytest
from django.http import HttpResponse, HttpRequest, JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from ..models import Product


@method_decorator(csrf_exempt, name='dispatch')
def delete_data_product_by_id(request, pk: int):
    if request.method == 'GET':
        # Récupérer les données du product
        product = Product.objects.get(id=pk)
        data = {
            'name': product.name,
            'details': product.details,
            'price': product.price,
            'description': product.description,
            'color': product.color,
            'stock': product.stock

            # Ajoutez d'autres champs au besoin
        }
        return JsonResponse(data)

    elif request.method == 'POST':
        # Supprimer le product avec l'ID spécifié
        product = Product.objects.get(id=pk)
        product.delete()
        return JsonResponse({'message': 'Les données du product ont été supprimées'})
