import json

from django.http import HttpResponse, HttpRequest, JsonResponse
from rest_framework.decorators import api_view

from ..models import Product


@api_view(["GET"])
def getAllProductsDetails(request):
    products = Product.objects.all()

    # Construire la liste des utilisateurs
    users_list = []
    for product in products:
        user_data = {
            'id': product.id,
            'name': product.name,
            'details': product.details,
            'price': product.price,
            'description': product.description,
            'color': product.color,
            'stock': product.stock
            # Ajoutez d'autres champs au besoin
        }
        users_list.append(user_data)

    # Retourner la liste au format JSON

    # Retourner la réponse avec le JSON formaté
    return JsonResponse(users_list, safe=False)
