from django.http import HttpResponse, HttpRequest, JsonResponse
from rest_framework.decorators import api_view

from ..models import Product


@api_view(["GET"])
def getProductDetailsById(request,pk: int):
    # Recherche du Product dans la base de données
    try:
        product = Product.objects.get(id=pk)
        data = {
            'name': product.name,
            'details': product.details,
            'price': product.price,
            'description': product.description,
            'color': product.color,
            'stock': product.stock
            # Ajoutez d'autres champs au besoin
        }
        return JsonResponse(data)
    except Product.DoesNotExist:
        return HttpResponse('Product non trouvé', status=404)
