import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from ..models import Product


@csrf_exempt
def update_data_product_by_id(request, pk: int):
    try:
        if request.method == 'GET':
            # Récupérer les données du product
            product = Product.objects.get(id=pk)
            data = {
                'name': product.name,
                'details': product.details,
                'price': product.price,
                'description': product.description,
                'color': product.color,
                'stock': product.stock
                # Ajoutez d'autres champs au besoin
            }
            return JsonResponse(data)

        elif request.method == 'POST':
            # Récupération des données de la requête POST
            product = Product.objects.get(id=pk)
            name = request.POST.get('name', product.name)
            details = request.POST.get('details', product.details)
            price = request.POST.get('product', product.price)
            description = request.POST.get('description', product.description)
            color = request.POST.get('color', product.color)
            stock = request.POST.get('stock', product.stock)


            # Mettre à jour les données du product existant
            product.name = name
            product.details = details
            product.price = price
            product.description = description
            product.color = color
            product.stock = stock

            product.save()

            # Retournez une réponse de succès
            return JsonResponse({'message': 'product mis à jour avec succès !'})
        else:
            return JsonResponse({'error': 'Méthode HTTP non autorisée'}, status=405)

    except Product.DoesNotExist:
        return JsonResponse({'error': 'product non trouvé'}, status=404)

    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)
